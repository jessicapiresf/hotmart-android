package android.tests.jessicafreitas.hotmart.repository;

import android.content.Context;
import android.tests.jessicafreitas.hotmart.domain.Messages;
import android.tests.jessicafreitas.hotmart.domain.Profile;
import android.tests.jessicafreitas.hotmart.domain.Sales;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Jessica on 19/06/16.
 */
public class HotmartRepositoryOnline implements IHotmartRepository {

    private Context mContext;

    /*Repositório destinado as requisições de serviços online.
    * Como não existe serviços online disponíveis no momento, foi simulado um retorno JSON,
    * disponível na pasta assets.*/

    public HotmartRepositoryOnline(Context context) {
        mContext = context;
    }

    @Override
    public Profile getProfile() {
        Profile profile;
        String jsonString = loadJSONFromAsset("profile.json");
        Gson gson = new Gson();
        profile = gson.fromJson(jsonString, Profile.class);
        return profile;
    }

    @Override
    public Messages[] getMessages() {
        Messages[] message;
        String jsonString = loadJSONFromAsset("messages.json");
        Gson gson = new Gson();
        message = gson.fromJson(jsonString, Messages[].class);
        return message;
    }

    @Override
    public Sales[] getSales() {
        Sales[] sales;
        String jsonString = loadJSONFromAsset("sales.json");
        Gson gson = new Gson();
        sales = gson.fromJson(jsonString, Sales[].class);
        return sales;
    }


    public String loadJSONFromAsset(String jsonUrl) {
        String json;
        try {
            InputStream is = mContext.getApplicationContext().getAssets().open(jsonUrl);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json.toString();
    }
}
