package android.tests.jessicafreitas.hotmart.repository;

import android.tests.jessicafreitas.hotmart.domain.*;
import android.tests.jessicafreitas.hotmart.persistence.PersistenceException;

/**
 * Created by Jessica on 19/06/16.
 */
public interface IHotmartRepository {

    public Profile getProfile() throws PersistenceException;
    public Messages[] getMessages() throws PersistenceException;
    public Sales[] getSales() throws PersistenceException;

}
