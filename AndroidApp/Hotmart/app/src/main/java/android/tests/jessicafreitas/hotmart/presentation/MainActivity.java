package android.tests.jessicafreitas.hotmart.presentation;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.tests.jessicafreitas.hotmart.R;
import android.tests.jessicafreitas.hotmart.domain.Profile;
import android.tests.jessicafreitas.hotmart.helper.ImageHelper;
import android.tests.jessicafreitas.hotmart.persistence.DatabaseHelper;
import android.tests.jessicafreitas.hotmart.presentation.fragment.MessagesFragment;
import android.tests.jessicafreitas.hotmart.presentation.fragment.ProfileFragment;
import android.tests.jessicafreitas.hotmart.presentation.fragment.SalesFragment;
import android.tests.jessicafreitas.hotmart.service.HotmartService;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Fragment fragment = null;
    FragmentTransaction transaction = null;
    HotmartService service = null;
    DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fragment = new ProfileFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainFrame, fragment);
        transaction.commit();

        dbHelper = new DatabaseHelper(getApplicationContext());
        dbHelper.prepareDataBase();
        setMenuInformations();

    }


    private void setMenuInformations() {
        service = new HotmartService(getApplicationContext());
        Profile profile = service.getProfile();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerLayout = navigationView.getHeaderView(0);

        TextView name = (TextView) headerLayout.findViewById(R.id.menu_username);
        TextView email = (TextView) headerLayout.findViewById(R.id.menu_useremail);
        ImageView picture = (ImageView) headerLayout.findViewById(R.id.menu_userpicture);

        name.setText(profile.getName());
        email.setText(profile.getEmail());

        if(service.isOnline())
            new ImageHelper.DownLoadImageTask(picture).execute(profile.getPicture());

        setMenuCounter(R.id.nav_affiliates, profile.getAffiliates());
        setMenuCounter(R.id.nav_messages, profile.getMessages());
        setMenuCounter(R.id.nav_notifications, profile.getNotifications());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_my_sales) {
            fragment = new SalesFragment();
        } else if (id == R.id.nav_my_products) {

        } else if (id == R.id.nav_affiliates) {

        } else if (id == R.id.nav_messages) {
            fragment = new MessagesFragment();
        } else if (id == R.id.nav_notifications) {

        } else if (id == R.id.nav_my_account) {
            fragment = new ProfileFragment();
        } else if (id == R.id.nav_about_app) {

        }

        if (fragment != null) {
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mainFrame, fragment);
            transaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }


    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    private void setMenuCounter(@IdRes int itemId, int count) {
        if (count > 0) {
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            LinearLayout layout = (LinearLayout) navigationView.getMenu().findItem(itemId).getActionView();
            TextView view = (TextView) layout.findViewById(R.id.badge);
            view.setText(count > 0 ? String.valueOf(count) : null);
            view.setVisibility(View.VISIBLE);
        }
    }
}
