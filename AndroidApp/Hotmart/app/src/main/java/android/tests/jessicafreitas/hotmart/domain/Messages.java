package android.tests.jessicafreitas.hotmart.domain;

import android.tests.jessicafreitas.hotmart.persistence.IPersistable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Jessica on 19/06/16.
 */

@DatabaseTable
public class Messages implements IPersistable {

    @DatabaseField
    public int id;
    @DatabaseField
    public String senderName;
    @DatabaseField
    public String senderPicture;

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderPicture() {
        return senderPicture;
    }

    public void setSenderPicture(String senderPicture) {
        this.senderPicture = senderPicture;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }
}
