package android.tests.jessicafreitas.hotmart.adapter;

import android.content.Context;
import android.tests.jessicafreitas.hotmart.R;
import android.tests.jessicafreitas.hotmart.domain.Sales;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Jessica on 21/06/16.
 */
public class SalesAdapter extends ArrayAdapter<Sales> {

    Context mContext;
    int layoutResourceId;
    Sales data[] = null;
    boolean isProfile;

    public SalesAdapter(Context context, int resource, Sales[] data, boolean profile) {
        super(context, resource, data);
        this.layoutResourceId = resource;
        this.mContext = context;
        this.data = data;
        this.isProfile = profile;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        Sales sales = data[position];

        TextView title = (TextView) convertView.findViewById(R.id.sales_row_title);
        TextView infos = (TextView) convertView.findViewById(R.id.sales_row_info);
        TextView value = (TextView) convertView.findViewById(R.id.sales_row_value);
        ImageView pending = (ImageView) convertView.findViewById(R.id.sales_row_pending);

        title.setText(sales.getTitle());
        infos.setText(String.format("%s - %s", sales.getId(), sales.getDate()));
        value.setText(sales.getValue());

        if(sales.isPending() && !isProfile)
            pending.setVisibility(View.VISIBLE);

        if(position%2 == 0){
            if(isProfile)
                convertView.setBackgroundResource(R.drawable.border_sales_profile_grey);
            else
                convertView.setBackgroundResource(R.drawable.border_sales_blue);
        }
        else{
            if(isProfile)
                convertView.setBackgroundResource(R.drawable.border_sales_profile_white);
            else
                convertView.setBackgroundResource(R.drawable.border_sales_grey);
        }

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
