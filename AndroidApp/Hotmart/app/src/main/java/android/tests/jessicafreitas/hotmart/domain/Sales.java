package android.tests.jessicafreitas.hotmart.domain;

import android.tests.jessicafreitas.hotmart.persistence.IPersistable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Jessica on 21/06/16.
 */

@DatabaseTable
public class Sales implements IPersistable {

    @DatabaseField
    int id;
    @DatabaseField
    String date;
    @DatabaseField
    String title;
    @DatabaseField
    boolean pending;
    @DatabaseField
    String value;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }
}
