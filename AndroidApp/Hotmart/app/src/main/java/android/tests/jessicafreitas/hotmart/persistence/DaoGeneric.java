package android.tests.jessicafreitas.hotmart.persistence;

import android.util.Log;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jessica on 21/06/16.
 */
public class DaoGeneric<T extends IPersistable> {

	private Dao<T, Long> dao;

	private DaoGeneric(Class<T> daoClass) throws PersistenceException {
		try {
			this.dao = PersistenceManager.getInstance().getHelper()
					.getDao(daoClass);
			
		} catch (SQLException e) {
			throw new android.tests.jessicafreitas.hotmart.persistence.PersistenceException("Falha ao criar DAO " + daoClass, e);
		}
	}

	/**
	 * Cria um DAO da classe informada
	 * 
	 * @param daoClass
	 * @return This
	 * @throws PersistenceException
	 */
	public static <T extends IPersistable> DaoGeneric<T> createDao(
			Class<T> daoClass) throws android.tests.jessicafreitas.hotmart.persistence.PersistenceException {
		return new DaoGeneric<T>(daoClass);
	}

	/**
	 * Salva ou atualiza o objeto
	 * 
	 * @param obj
	 * @return id
	 * @throws PersistenceException
	 */
	public synchronized long saveOrUpdate(T obj) throws android.tests.jessicafreitas.hotmart.persistence.PersistenceException {
		try {
			dao.createOrUpdate(obj);
			return obj.getId();
		} catch (Exception e) {
			Log.e("error", e.getMessage(), e);
			throw new android.tests.jessicafreitas.hotmart.persistence.PersistenceException("Falha ao salvar objeto " + obj, e);
		} finally {
			
			dao.clearObjectCache();
		
		}
	}

	/**
	 * Remove do banco o objeto informado
	 * 
	 * @param obj
	 * @return quantidade de objetos removidos
	 * @throws PersistenceException
	 */
	public int delete(T obj) throws android.tests.jessicafreitas.hotmart.persistence.PersistenceException {
		try {
			return dao.delete(obj);
		} catch (Exception e) {
			throw new android.tests.jessicafreitas.hotmart.persistence.PersistenceException("Falha ao deletar objeto " + obj, e);
		}
	}

	/**
	 * Remove do banco o objeto dado o id informado
	 * 
	 * @param id
	 * @return quantidade de objetos removidos
	 * @throws PersistenceException
	 */
	public int deleteById(long id) throws android.tests.jessicafreitas.hotmart.persistence.PersistenceException {
		try {
			return dao.deleteById(id);
		} catch (SQLException e) {
			throw new android.tests.jessicafreitas.hotmart.persistence.PersistenceException("Falha ao deletar objeto " + id, e);
		}
	}

	/**
	 * Remove do banco a lista de objetos dado os ids informados
	 * 
	 * @param List
	 *            ids
	 * @return quantidade de objetos removidos
	 * @throws PersistenceException
	 */
	public int deleteIds(List<Long> obj) throws android.tests.jessicafreitas.hotmart.persistence.PersistenceException {
		try {
			return dao.deleteIds(obj);
		} catch (Exception e) {
			throw new android.tests.jessicafreitas.hotmart.persistence.PersistenceException("Falha ao deletar objetos by id", e);
		}
	}

	/**
	 * Remove do banco a lista de objetos informados
	 * 
	 * @param List
	 *            obj
	 * @return quantidade de objetos removidos
	 * @throws PersistenceException
	 */
	public int delete(List<T> obj) throws android.tests.jessicafreitas.hotmart.persistence.PersistenceException {
		try {
			return dao.delete(obj);
		} catch (Exception e) {
			throw new android.tests.jessicafreitas.hotmart.persistence.PersistenceException("Falha ao deletar lista de objetos",
					e);
		}
	}

	/**
	 * Retorna a quantidade de objetos contidos no banco de dados
	 * 
	 * @return long
	 * @throws PersistenceException
	 */
	public long size() {
		try {
			return dao.countOf();
		} catch (SQLException e) {
			return 0;
		}
	}

	/**
	 * Retorna o objeto dado o id
	 * 
	 * @param id
	 * @return obj
	 * @throws PersistenceException
	 */
	public T getObjectById(long id) {
		try {
			return dao.queryForId(id);
		} catch (SQLException e) {
			return null;
		}
	}

	/**
	 * Retorna o primeiro objeto encontrado
	 * 
	 * @param orderBy
	 * @param ascending
	 * @return obj
	 * @throws PersistenceException
	 */
	public T getFirstObject(String[] orderBy, boolean ascending) {
		try {
			QueryBuilder<T, Long> queryBuilder = dao.queryBuilder();

			if (orderBy != null) {
				for (String columnName : orderBy) {
					queryBuilder.orderBy(columnName, ascending);
				}
			}

			return queryBuilder.queryForFirst();
		} catch (SQLException e) {
			return null;
		}
	}

	/**
	 * Retorna o QueryBuider para se fazer queries mais especificas
	 * 
	 * @return QueryBuilder
	 */
	public QueryBuilder<T, Long> getQueryBuilder() {
		return dao.queryBuilder();
	}

	/**
	 * Retorna todos os objetos contidos no banco de dados
	 * 
	 * @return List Obj
	 * @throws PersistenceException
	 */
	public List<T> getAll() {
		try {
			return dao.queryForAll();
		} catch (SQLException e) {
			return new ArrayList<T>();
		}
	}
}
