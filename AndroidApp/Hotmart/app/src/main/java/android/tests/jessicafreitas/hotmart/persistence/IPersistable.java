package android.tests.jessicafreitas.hotmart.persistence;

/**
 * Created by Jessica on 21/06/16.
 */
public interface IPersistable {

	int getId();

	void setId(int id);

}
