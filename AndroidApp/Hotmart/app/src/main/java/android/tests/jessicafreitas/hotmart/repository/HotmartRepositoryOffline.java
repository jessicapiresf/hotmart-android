package android.tests.jessicafreitas.hotmart.repository;

import android.content.Context;
import android.database.SQLException;
import android.tests.jessicafreitas.hotmart.domain.Messages;
import android.tests.jessicafreitas.hotmart.domain.Profile;
import android.tests.jessicafreitas.hotmart.domain.Sales;
import android.tests.jessicafreitas.hotmart.persistence.DaoGeneric;
import android.tests.jessicafreitas.hotmart.persistence.PersistenceException;

import java.util.ArrayList;

/**
 * Created by Jessica on 19/06/16.
 */
public class HotmartRepositoryOffline implements IHotmartRepository {

    private Context mContext;

    public HotmartRepositoryOffline(Context context) {
        mContext = context;
    }

    @Override
    public Profile getProfile() throws SQLException, PersistenceException{
        DaoGeneric<Profile> dao = DaoGeneric.createDao(Profile.class);
        ArrayList<Profile> profile = (ArrayList<Profile>) dao.getAll();
        return profile.get(0);
    }

    @Override
    public Messages[] getMessages() throws SQLException, PersistenceException {
        DaoGeneric<Messages> dao = DaoGeneric.createDao(Messages.class);
        ArrayList<Messages> messages = (ArrayList<Messages>) dao.getAll();
        return messages.toArray(new Messages[messages.size()]);
    }

    @Override
    public Sales[] getSales() throws SQLException, PersistenceException {
        DaoGeneric<Sales> dao = DaoGeneric.createDao(Sales.class);
        ArrayList<Sales> sales = (ArrayList<Sales>) dao.getAll();
        return sales.toArray(new Sales[sales.size()]);
    }
}
