package android.tests.jessicafreitas.hotmart.presentation.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.tests.jessicafreitas.hotmart.R;
import android.tests.jessicafreitas.hotmart.domain.Messages;
import android.tests.jessicafreitas.hotmart.helper.ImageHelper;
import android.tests.jessicafreitas.hotmart.presentation.MainActivity;
import android.tests.jessicafreitas.hotmart.service.HotmartService;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Jessica on 19/06/16.
 */
public class MessagesFragment extends Fragment {


    HotmartService service = null;
    View myView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.fragment_title_messages));
        View view =  inflater.inflate(R.layout.fragment_messages, container, false);
        myView = view;

        setContent();
        return view;
    }

    private void setContent() {

        service = new HotmartService(getContext());
        Messages[] messages;
        messages = service.getMessages();
        LinearLayout layoutInline = null;

        int[] colors = { R.drawable.circle_colorful_black, R.drawable.profile_image, R.drawable.circle_colorful_mandy, R.drawable.circle_colorful_havelockblue, R.drawable.circle_colorful_daisybrush, R.drawable.circle_colorful_trinidad};
        for (int i = 0; i < messages.length; i++) {

            if(i%4==0){
                layoutInline = new LinearLayout(getContext());
                layoutInline.setOrientation(LinearLayout.HORIZONTAL);
                layoutInline.setGravity(Gravity.CENTER_HORIZONTAL);
                ((LinearLayout) myView.findViewById(R.id.contact_list_profile_inline)).addView(layoutInline);
            }

            View layoutContact = View.inflate(getContext(), R.layout.message_contact, null);
            LinearLayout.LayoutParams ladderFLParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
            layoutContact.setLayoutParams(ladderFLParams);
            TextView name = (TextView) layoutContact.findViewById(R.id.contact_name);
            name.setText(messages[i].getSenderName());

            ImageView image = (ImageView) layoutContact.findViewById(R.id.contact_image);
            TextView noImage = (TextView) layoutContact.findViewById(R.id.contact_noimage);
            if(messages[i].getSenderPicture() != "" && service.isOnline()){
                image.setVisibility(View.VISIBLE);
                image = (ImageView) layoutContact.findViewById(R.id.contact_image);
                new ImageHelper.DownLoadImageTask(image).execute(messages[i].getSenderPicture());
            }
            else{
                noImage.setVisibility(View.VISIBLE);
                int color = colors[i % colors.length];
                noImage.setBackgroundResource(color);
                noImage.setText(messages[i].getSenderName().toUpperCase().substring(0,1));
            }

            layoutInline.addView(layoutContact);
        }
    }
}
