package android.tests.jessicafreitas.hotmart.presentation.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.tests.jessicafreitas.hotmart.R;
import android.tests.jessicafreitas.hotmart.adapter.SalesAdapter;
import android.tests.jessicafreitas.hotmart.domain.Sales;
import android.tests.jessicafreitas.hotmart.presentation.MainActivity;
import android.tests.jessicafreitas.hotmart.service.HotmartService;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Created by Jessica on 19/06/16.
 */
public class SalesFragment extends Fragment {

    HotmartService service = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.fragment_title_sales));
        View view =  inflater.inflate(R.layout.fragment_sales, container, false);
        service = new HotmartService(getContext());
        Sales[] sales = service.getSales();
        SalesAdapter adapter = new SalesAdapter(getContext(), R.layout.sales_row, sales, false);
        ListView lv = (ListView) view.findViewById(R.id.listSales);
        lv.setAdapter(adapter);

        return view;

    }
}
