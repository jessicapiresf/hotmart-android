package android.tests.jessicafreitas.hotmart.persistence;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jessica on 21/06/16.
 */
public class PersistenceManager {

	private List<Class<? extends IPersistable>> classModels = new ArrayList<Class<? extends IPersistable>>();
	private DatabaseHelper helper;

	private static PersistenceManager instance = null;

	/**
	 * Retorna a instancia da classe
	 * 
	 * @return this
	 */
	public static PersistenceManager getInstance() {
		if (instance == null) {
			instance = new PersistenceManager();
		}
		return instance;
	}

	/**
	 * Fecha o banco de dados
	 */
	public void closeDatabase() {
		helper.close();
	}

	/**
	 * Dropa e recria todas as tabelas do banco
	 */
	public void clearAllData() {
		helper.clearAllData();
	}

	/**
	 * Retorna o gerenciador do ORM Lite
	 * 
	 * @return DatabaseHelper
	 */
	public DatabaseHelper getHelper() {
		return helper;
	}

	/**
	 * Adiciona uma classe que extende IPersistable para ser criada no banco de
	 * dados
	 * 
	 * @param model
	 */
	public void addClassModel(Class<? extends IPersistable> model) {
		classModels.add(model);
	}

	/**
	 * Cria as tabelas no banco se as mesmas nao existirem
	 * 
	 * @param context
	 * @param databaseName
	 * @param version
	 */
	public void createTablesIfNotExists(Context context, String databaseName, int version) {
		helper = new DatabaseHelper(context, databaseName, version, classModels);
	}

}
