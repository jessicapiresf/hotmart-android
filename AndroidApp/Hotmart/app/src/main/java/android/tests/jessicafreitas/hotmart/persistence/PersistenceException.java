package android.tests.jessicafreitas.hotmart.persistence;


/**
 * Created by Jessica on 21/06/16.
 */
public class PersistenceException extends Exception {

	public PersistenceException(String string, Exception e) {
		super(string, e);
	}

}
