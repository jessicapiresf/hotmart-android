package android.tests.jessicafreitas.hotmart.presentation.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.tests.jessicafreitas.hotmart.R;
import android.tests.jessicafreitas.hotmart.adapter.SalesAdapter;
import android.tests.jessicafreitas.hotmart.domain.Messages;
import android.tests.jessicafreitas.hotmart.domain.Profile;
import android.tests.jessicafreitas.hotmart.domain.Sales;
import android.tests.jessicafreitas.hotmart.helper.ImageHelper;
import android.tests.jessicafreitas.hotmart.presentation.MainActivity;
import android.tests.jessicafreitas.hotmart.service.HotmartService;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Jessica on 19/06/16.
 */
public class ProfileFragment extends Fragment {

    HotmartService service = null;
    View myView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.fragment_title_profile));
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        myView = view;

        HotmartService service = new HotmartService(getContext());
        Profile profile = service.getProfile();

        TextView tv = (TextView) view.findViewById(R.id.profile_sales_value);
        tv.setText(profile.getBalance());

        setMessages();
        setSales();

        return view;
    }

    private void setSales() {
        service = new HotmartService(getContext());
        Sales[] sales = service.getSales();

        SalesAdapter adapter = new SalesAdapter(getContext(), R.layout.sales_row, sales, true);

        ListView lv = (ListView) myView.findViewById(R.id.listSales);
        lv.setAdapter(adapter);
    }

    private void setMessages() {
        service = new HotmartService(getContext());
        Messages[] messages = service.getMessages();

        TextView messageContou = (TextView) myView.findViewById(R.id.messages_cont);
        int messageQuantity = service.getProfile().getMessages();
        if(messageQuantity > 0)
            messageContou.setVisibility(View.VISIBLE);
        if(messageQuantity > 0 && messageQuantity < 10)
            messageContou.setText(Integer.toString(messageQuantity));
        else
            messageContou.setText("+10");

        int[] colors = { R.drawable.circle_colorful_black, R.drawable.profile_image, R.drawable.circle_colorful_mandy, R.drawable.circle_colorful_havelockblue, R.drawable.circle_colorful_daisybrush, R.drawable.circle_colorful_trinidad};
        for (int i = 0; i < messages.length; i++) {

            View layoutContact = View.inflate(getContext(), R.layout.message_contact, null);
            TextView name = (TextView) layoutContact.findViewById(R.id.contact_name);
            name.setText(messages[i].getSenderName());

            ImageView image = (ImageView) layoutContact.findViewById(R.id.contact_image);
            TextView noImage = (TextView) layoutContact.findViewById(R.id.contact_noimage);
            if(messages[i].getSenderPicture() != "" && service.isOnline()){
                image.setVisibility(View.VISIBLE);
                image = (ImageView) layoutContact.findViewById(R.id.contact_image);
                new ImageHelper.DownLoadImageTask(image).execute(messages[i].getSenderPicture());
            }
            else{
                noImage.setVisibility(View.VISIBLE);
                int color = colors[i % colors.length];
                noImage.setBackgroundResource(color);
                noImage.setText(messages[i].getSenderName().toUpperCase().substring(0,1));

            }
            ((LinearLayout) myView.findViewById(R.id.contact_list_profile)).addView(layoutContact);
        }
    }
}
