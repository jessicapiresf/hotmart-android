package android.tests.jessicafreitas.hotmart.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.tests.jessicafreitas.hotmart.domain.Messages;
import android.tests.jessicafreitas.hotmart.domain.Profile;
import android.tests.jessicafreitas.hotmart.domain.Sales;
import android.util.Log;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Jessica on 21/06/16.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

	private List<Class<? extends IPersistable>> classModels;
	Context mContext;
	private static final String DATABASE_NAME = "hotmart.db";
	private static final String DATABASE_PATH = "/data/data/android.tests.jessicafreitas.hotmart/databases/";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_PATH+DATABASE_NAME, null, 1);
		mContext = context;
		boolean dbexist = checkdatabase();
		if (!dbexist) {

			// If database did not exist, try copying existing database from assets folder.
			try {
				File dir = new File(DATABASE_PATH);
				dir.mkdirs();
				InputStream myinput = context.getAssets().open(DATABASE_NAME);
				String outfilename = DATABASE_PATH + DATABASE_NAME;
				Log.i(DatabaseHelper.class.getName(), "DB Path : " + outfilename);
				OutputStream myoutput = new FileOutputStream(outfilename);
				byte[] buffer = new byte[1024];
				int length;
				while ((length = myinput.read(buffer)) > 0) {
					myoutput.write(buffer, 0, length);
				}
				myoutput.flush();
				myoutput.close();
				myinput.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public DatabaseHelper(Context context, String databaseName, int version, List<Class<? extends IPersistable>> classModels){
		super(context, databaseName, null, version);
		mContext = context;
		this.classModels = classModels;
	}

	/*
	* Check whether or not database exist
	*/
	private boolean checkdatabase() {
		boolean checkdb = false;

		String myPath = DATABASE_PATH + DATABASE_NAME;
		File dbfile = new File(myPath);
		checkdb = dbfile.exists();
		Log.i(DatabaseHelper.class.getName(), "DB Exist : " + checkdb);
		return checkdb;
	}

	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		for (Class<? extends IPersistable> dataClass : classModels) {
			try {
				TableUtils.createTableIfNotExists(connectionSource, dataClass);
			} catch (SQLException e) {
				Log.e("Falha ao criar tabela " + dataClass.getSimpleName(), e.getMessage());
			}
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		for (Class<? extends IPersistable> dataClass : classModels) {
			try {
				TableUtils.dropTable(connectionSource, dataClass, true);
			} catch (SQLException e) {
				//Log.e("Falha ao remover tabela " + dataClass.getSimpleName(), e.getMessage());
			}
		}
		onCreate(db, connectionSource);
	}
	
	/**
	 * Dropa e recria todas as tabelas do banco
	 */
	public void clearAllData(){
		for (Class<? extends IPersistable> dataClass : classModels) {
			try {
				TableUtils.dropTable(connectionSource, dataClass, true);
				TableUtils.createTableIfNotExists(connectionSource, dataClass);
			} catch (SQLException e) {
				Log.e("Falha ao limpar tabela " + dataClass.getSimpleName(), e.getMessage());
			}
		}
	}

	public void prepareDataBase() {
		PersistenceManager.getInstance().addClassModel(Profile.class);
		PersistenceManager.getInstance().addClassModel(Messages.class);
		PersistenceManager.getInstance().addClassModel(Sales.class);
		PersistenceManager.getInstance().createTablesIfNotExists(
				mContext, "hotmart.db", 1);
	}
}
