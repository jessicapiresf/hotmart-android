package android.tests.jessicafreitas.hotmart.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.tests.jessicafreitas.hotmart.domain.Messages;
import android.tests.jessicafreitas.hotmart.domain.Profile;
import android.tests.jessicafreitas.hotmart.domain.Sales;
import android.tests.jessicafreitas.hotmart.persistence.PersistenceException;
import android.tests.jessicafreitas.hotmart.repository.HotmartRepositoryOffline;
import android.tests.jessicafreitas.hotmart.repository.HotmartRepositoryOnline;
import android.tests.jessicafreitas.hotmart.repository.IHotmartRepository;

/**
 * Created by Jessica on 19/06/16.
 */
public class HotmartService {

    private Context mContext;
    private IHotmartRepository repository;

    public HotmartService(Context context) {
        mContext = context;
    }

    public IHotmartRepository getRepository(boolean isOnline){

        /*Verifica se o dispositivo está conectado à internet,
        caso esteja, ele vai buscar os dados do JSON (simulando uma chamada de serviço).
        Se estiver offline, ele vai buscar os dados do banco de dados local.
         */

        if (isOnline) {
            if (isOnline())
                repository = new HotmartRepositoryOnline(mContext);
            else
                repository = new HotmartRepositoryOffline(mContext);
        }
        return repository;
    }

    public Profile getProfile(){
        Profile p = null;
        try {
            p =  getRepository(true).getProfile();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return p;
    }

    public Messages[] getMessages(){
        Messages[] p = null;
        try {
            p = getRepository(true).getMessages();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return p;
    }

    public Sales[] getSales(){
        Sales[] p = null;
        try {
            p = getRepository(true).getSales();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return p;
    }


    public boolean isOnline()
    {
        ConnectivityManager conectivtyManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
