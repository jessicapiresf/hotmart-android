package android.tests.jessicafreitas.hotmart.domain;

import android.tests.jessicafreitas.hotmart.persistence.IPersistable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Jessica on 19/06/16.
 */

@DatabaseTable
public class Profile implements IPersistable {

    @DatabaseField
    public int id;
    @DatabaseField
    public String name;
    @DatabaseField
    public String email;
    @DatabaseField
    public String balance;
    @DatabaseField
    public String picture;
    @DatabaseField
    public int newAffiliates;
    @DatabaseField
    public int newMessages;
    @DatabaseField
    public int newNotifications;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getAffiliates() {
        return newAffiliates;
    }

    public void setAffiliates(int affiliates) {
        this.newAffiliates = affiliates;
    }

    public int getMessages() {
        return newMessages;
    }

    public void setMessages(int messages) {
        this.newMessages = messages;
    }

    public int getNotifications() {
        return newNotifications;
    }

    public void setNotifications(int notifications) {
        this.newNotifications = notifications;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }
}
