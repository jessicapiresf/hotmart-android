<h2>README</h2>
<p>Siga os passos para clonar o repositório e deixá-lo pronto para iniciar o trabalho.</p>


<h5>Este repositório serve para?</h5>
<p>Este é o repositório virtual para a criação de um app teste da empresa Hotmart.</p>


<h5>Como fazer para configurar?</h5>
- Instalação do ambiente Android (Android Studio/Android SDK)


<h5>Tecnologias utilizadas:</h5>
- Android
- JAVA
- SQLite (ORMLite)
- JSON (Gson)

<h5>Arquitetura:</h5>
- Apresentação (activities e fragments)
- Camada de serviços
- Camada de repositórios  - online (JSON local simulando uma chamada de serviço - utilizado a lib Gson) e offline (banco de dados local)
- Persistencia (utlizado a lib ORMlite)
- Domínios
- Helper
- Adapter


<h5>Com quem devo falar?</h5>
Jéssica Pires de Freitas é a proprietária deste repositório.
Contato - jessicapiresf@gmail.com
